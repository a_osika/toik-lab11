package com.example.movies.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter

public class MovieDto {
    private int movieId;
    private String title;
    private int year;
    private String image;
}
