package com.example.movies.service.impl;

import com.example.movies.dto.MovieDto;
import com.example.movies.repository.MovieRepository;
import com.example.movies.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    public Map<String, List<MovieDto>> getAllMovies(){
        Map<String, List<MovieDto>> moviesMap = new HashMap<>();
        moviesMap.put("movies", movieRepository.getMovies());
        return moviesMap;
    }
}
