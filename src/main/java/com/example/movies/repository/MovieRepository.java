package com.example.movies.repository;

import com.example.movies.dto.MovieDto;

import java.util.List;

public interface MovieRepository {
    List<MovieDto> getMovies();
}
