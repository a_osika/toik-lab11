package com.example.movies.rest;

import com.example.movies.dto.MovieDto;
import com.example.movies.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class MovieApiController {

    private final MovieService movieService;

    @Autowired
    public MovieApiController(MovieService movieService) {
        this.movieService = movieService;
    }

    @CrossOrigin
    @GetMapping(value = "/movies")
    public ResponseEntity<Map<String, List<MovieDto>>> allMovies() {
        Map<String, List<MovieDto>> moviesMap = movieService.getAllMovies();
        return new ResponseEntity<>(moviesMap, HttpStatus.OK);
    }
}
